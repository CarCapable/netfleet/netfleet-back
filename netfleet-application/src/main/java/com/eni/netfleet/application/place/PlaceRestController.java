package com.eni.netfleet.application.place;

import com.eni.netfleet.metier.place.bean.PlaceBean;
import com.eni.netfleet.metier.place.service.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/places")
public class PlaceRestController {

    private PlaceService placeService;

    public PlaceRestController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping
    public List<PlaceBean> getAllPlaces() {
        return placeService.getAllPlaces();
    }
}
