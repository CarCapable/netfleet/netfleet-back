package com.eni.netfleet.application.vehicle;

import com.eni.netfleet.metier.vehicle.bean.DisableVehicleBean;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import com.eni.netfleet.metier.vehicle.service.VehicleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/vehicles")
public class VehicleRestController {

    private VehicleService vehicleService;

    public VehicleRestController(VehicleService vehicleService) {

        this.vehicleService = vehicleService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<VehicleBean> getAllVehicles(@RequestParam(value = "search", required = false) String search) {
        return vehicleService.getAllVehicles(search);
    }

    @GetMapping("/{idVehicle}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public VehicleBean getVehicleById(@PathVariable(name = "idVehicle") Long idVehicle) {

        return vehicleService.getVehicleById(idVehicle);
    }

    @PostMapping("/{idVehicle}/disabled")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public void disabledVehicleById(@PathVariable(name = "idVehicle") Long idVehicle, @RequestBody final DisableVehicleBean disableVehicleBean) {
        vehicleService.disabledVehicleById(idVehicle, disableVehicleBean);
    }

    @GetMapping("/available")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<VehicleBean> getAllVehicleAvailable(@RequestParam(value = "search", required = false) String search) {
        return vehicleService.getAllVehiclesAvailable(search);
    }

    @PostMapping("/newVehicle")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public void getAddNewVehicle(@RequestBody VehicleBean vehicleBean) {

        vehicleService.createVehicule(vehicleBean);
    }

    @PostMapping("/updateVehicle")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public void updateVehicule(@RequestBody VehicleBean vehicleBean) {
        vehicleService.updateVehicule(vehicleBean);
    }
}

