package com.eni.netfleet.application.carpoolRequest;

import com.eni.netfleet.metier.carpoolRequest.bean.CarpoolRequestBean;
import com.eni.netfleet.metier.carpoolRequest.service.CarpoolRequestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/carpoolRequests")
public class CarpoolRequestRestController {

    private CarpoolRequestService carpoolRequestService;

    public CarpoolRequestRestController(CarpoolRequestService carpoolRequestService) {
        this.carpoolRequestService = carpoolRequestService;
    }

    @GetMapping
    public List<CarpoolRequestBean> getAllCarpoolRequests() {
        return carpoolRequestService.getAllCarpoolRequests();
    }
}
