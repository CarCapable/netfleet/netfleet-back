package com.eni.netfleet.application.identity;

import com.eni.netfleet.metier.identity.bean.IdentityBean;
import com.eni.netfleet.metier.identity.service.IdentityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/identities")
public class IdentityRestController {

    private IdentityService identityService;

    public IdentityRestController(IdentityService identityService) {
        this.identityService = identityService;
    }

    @GetMapping
    public List<IdentityBean> getAllIdentities() { return identityService.getAllIdentities(); }
}
