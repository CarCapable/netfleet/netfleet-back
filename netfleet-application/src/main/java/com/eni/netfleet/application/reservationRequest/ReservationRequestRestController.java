package com.eni.netfleet.application.reservationRequest;

import com.eni.netfleet.metier.reservationRequest.bean.*;
import com.eni.netfleet.metier.reservationRequest.service.ReservationRequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/reservationRequests")
public class ReservationRequestRestController {

    private ReservationRequestService reservationRequestService;

    public ReservationRequestRestController(ReservationRequestService reservationRequestService) {
        this.reservationRequestService = reservationRequestService;
    }

    @PostMapping("/addReservationRequest")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<String> addRequestReservation(@RequestBody ReservationRequestAddBean reservationRequestAddBean) {
        return reservationRequestService.addRequestReservation(reservationRequestAddBean);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<ReservationRequestBean> getAllReservationRequests(@RequestParam(value = "search", required = false) String search) {
        return reservationRequestService.getAllReservationRequests(search);
    }

    @GetMapping("/{idUser}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<ReservationRequestBean> getAllReservationRequestsForUser(@PathVariable(name = "idUser") Long idUser, @RequestParam(value = "search", required = false) String search) {
        return reservationRequestService.getAllReservationRequestsForUser(idUser, search);
    }

    @GetMapping("/vehicle/{idVehicle}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<ReservationRequestInfoBean> getAllReservationRequestsForVehicle(@PathVariable(name = "idVehicle") Long idVehicle) {
        return reservationRequestService.getAllDateReservationForVehicle(idVehicle);
    }

    @GetMapping("/{idVehicle}/users")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<ResponseUserWhosReserveVehicle> getAllUserWhosReservedThisVehicle(@PathVariable(name = "idVehicle") Long idVehicle) {
        return reservationRequestService.getAllUserWhosReservedThisVehicle(idVehicle);
    }

    @GetMapping("/reservationAlreadyExisting")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<ReservationRequestBean> getAllReservationAlreadyExisting(@RequestBody ReservationRequestSearchBean reservationRequestSearchBean) {
        return reservationRequestService.getAllReservationAlreadyExisting(reservationRequestSearchBean);
    }

    @PostMapping("/carpoolExisting")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public List<ReservationRequestBean> getAllCarpoolExisting(@RequestBody ReservationRequestCarpoolSearchBean reservationRequestCarpoolSearchBean) {
        return reservationRequestService.getAllCarpoolExisting(reservationRequestCarpoolSearchBean);
    }

    @PostMapping("/addCarpool")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public void addCarpool(@RequestBody addCarpoolBean addCarpoolBean) {
        reservationRequestService.addCarpool(addCarpoolBean);
    }

}
