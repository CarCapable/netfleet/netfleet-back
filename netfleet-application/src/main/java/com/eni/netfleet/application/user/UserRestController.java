package com.eni.netfleet.application.user;

import com.eni.netfleet.metier.user.bean.UserBean;
import com.eni.netfleet.metier.user.constant.ERole;
import com.eni.netfleet.metier.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/rest/users")
public class UserRestController {

    private final Logger log = LoggerFactory.getLogger(UserRestController.class);

    private UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<UserBean>> getAllUsers() {
        List<UserBean> result = userService.getAllUsers();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/user/{idUser}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getUserById(@PathVariable(name = "idUser") Long idUser) {
        if (Objects.nonNull(idUser)) {
            return ResponseEntity.ok().body(userService.getUserById(idUser));
        } else {
            return ResponseEntity.badRequest().body("L'id de l'utilisateur ne doit pas êre null");
        }
    }

    @GetMapping("/{role}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllUsersByRole(@PathVariable(name = "role") String role) {
        if (Objects.isNull(role) || Arrays.stream(ERole.values())
                .noneMatch(e -> e.name().equals(role))) {
            return ResponseEntity.badRequest().body("Nom de rôle non valide");
        } else {
            return ResponseEntity.ok().body(userService.getAllUsersByRole(role));
        }
    }

    @PostMapping("/newUser")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void createUser(@RequestBody UserBean user) {
        log.info("Request to create user: {}", user);
        userService.createUser(user);
    }

    @PostMapping("/updateUser")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MOD')")
    public void updateUser(@RequestBody UserBean userBean) {
        userService.updateUser(userBean);
    }
}
