package com.eni.netfleet.application.userParameter;

import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;
import com.eni.netfleet.metier.userParameter.service.UserParameterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/userParameters")
public class UserParameterRestController {

    private UserParameterService userParameterService;

    public UserParameterRestController(UserParameterService userParameterService) { this.userParameterService = userParameterService; }

    @GetMapping
    public List<UserParameterBean> getAllUserParameters() { return userParameterService.getAllUserParameters(); }
}
