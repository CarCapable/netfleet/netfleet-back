package com.eni.netfleet.application.site;

import com.eni.netfleet.metier.site.bean.SiteBean;
import com.eni.netfleet.metier.site.service.SiteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/sites")
@RequiredArgsConstructor
public class SiteController {

    private final SiteService siteService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<SiteBean>> getAllSites() {
        List<SiteBean> result = siteService.getAllSites();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/sans-vehicle")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<SiteBean>> getAllSitesWihtoutVehicle() {
        List<SiteBean> result = siteService.getAllSitesWihtoutVehicule();
        return ResponseEntity.ok().body(result);
    }
}
