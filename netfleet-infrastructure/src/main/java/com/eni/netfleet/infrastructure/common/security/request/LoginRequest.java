package com.eni.netfleet.infrastructure.common.security.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginRequest {
    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
