package com.eni.netfleet.infrastructure.common.userParameter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserParameterRepository extends JpaRepository<UserParameterEntity, Long> {

    List<UserParameterEntity> findAllByUserId(Long idUser);
}
