package com.eni.netfleet.infrastructure.common.carpoolRequest.adapter;

import com.eni.netfleet.infrastructure.common.carpoolRequest.repository.CarpoolRequestEntity;
import com.eni.netfleet.infrastructure.common.carpoolRequest.repository.CarpoolRequestRepository;
import com.eni.netfleet.infrastructure.common.reservationRequest.adapter.ReservationRequestReadAdapter;
import com.eni.netfleet.infrastructure.common.user.adapter.UserReadAdapter;
import com.eni.netfleet.metier.carpoolRequest.bean.CarpoolRequestBean;
import com.eni.netfleet.metier.carpoolRequest.port.CarpoolRequestReadPort;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Validated
public class CarpoolRequestReadAdapter implements CarpoolRequestReadPort {

    private CarpoolRequestRepository carpoolRequestRepository;

    public CarpoolRequestReadAdapter(final CarpoolRequestRepository carpoolRequestRepository) {
        this.carpoolRequestRepository = carpoolRequestRepository;
    }

    @Override
    public List<CarpoolRequestBean> getAllCarpoolRequests() {
        List<CarpoolRequestEntity> carpoolRequestEntities = carpoolRequestRepository.findAll();
        return carpoolRequestEntities.stream()
                .map(CarpoolRequestReadAdapter::mapToCarpoolRequestBean)
                .collect(Collectors.toList());
    }

    public static CarpoolRequestBean mapToCarpoolRequestBean(CarpoolRequestEntity carpoolRequestEntity) {
        return CarpoolRequestBean.builder()
                .id(carpoolRequestEntity.getId())
                .createDate(carpoolRequestEntity.getCreateDate())
                .updateDate(carpoolRequestEntity.getUpdateDate())
                .applicant(UserReadAdapter.mapToUserBean(carpoolRequestEntity.getApplicant()))
                .reservationRequest(ReservationRequestReadAdapter.mapToReservationRequestBean(carpoolRequestEntity.getReservationRequest()))
                .build();
    }
}
