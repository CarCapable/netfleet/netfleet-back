package com.eni.netfleet.infrastructure.common.security.controller;

import com.eni.netfleet.infrastructure.common.identity.repository.IdentityEntity;
import com.eni.netfleet.infrastructure.common.security.repository.RoleRepository;
import com.eni.netfleet.infrastructure.common.security.repository.UserRepository;
import com.eni.netfleet.infrastructure.common.security.repository.entity.Role;
import com.eni.netfleet.metier.user.constant.ERole;
import com.eni.netfleet.infrastructure.common.security.request.LoginRequest;
import com.eni.netfleet.infrastructure.common.security.request.SignupRequest;
import com.eni.netfleet.infrastructure.common.security.response.JwtResponse;
import com.eni.netfleet.infrastructure.common.security.response.MessageResponse;
import com.eni.netfleet.infrastructure.common.security.security.jwt.JwtUtils;
import com.eni.netfleet.infrastructure.common.security.security.services.UserDetailsImpl;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private AuthenticationManager authenticationManager;

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder encoder;

    private JwtUtils jwtUtils;

    private static final String ERROR_ROLE_NOT_FOUND = "Error: Role is not found.";

    public AuthController(AuthenticationManager authenticationManager,
                          UserRepository userRepository,
                          RoleRepository roleRepository,
                          PasswordEncoder encoder,
                          JwtUtils jwtUtils) {

        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping(value = "/signin", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities()
                .stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(JwtResponse.builder()
                .token(jwt)
                .id(userDetails.getId())
                .username(userDetails.getUsername())
                .email(userDetails.getEmail())
                .roles(roles)
                .build());
    }

    @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest()
                    .body(MessageResponse.builder()
                            .message("Error: Username is already taken!"));
        }

        if (userRepository.ifExistsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest()
                    .body(MessageResponse.builder()
                            .message("Error: Email is already in use!"));
        }

        //Create new user(s identity
        IdentityEntity identityEntity = IdentityEntity.builder()
                .email(signUpRequest.getEmail())
                .firstName(signUpRequest.getFirstName())
                .name(signUpRequest.getName())
                .telephone(signUpRequest.getTelephone())
                .build();

        identityEntity.setCreateDate(java.sql.Date.valueOf(LocalDate.now()));
        identityEntity.setUpdateDate(java.sql.Date.valueOf(LocalDate.now()));

        // Create new user's account
        UserEntity user = UserEntity.builder()
                .username(signUpRequest.getUsername())
                .password(encoder.encode(signUpRequest.getPassword()))
                .identity(identityEntity)
                .status(true)
                .build();

        user.setCreateDate(java.sql.Date.valueOf(LocalDate.now()));
        user.setUpdateDate(java.sql.Date.valueOf(LocalDate.now()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);

        userRepository.save(user);

        return ResponseEntity.ok(MessageResponse.builder()
                .message("User registered successfully!")
                .build());
    }
}
