package com.eni.netfleet.infrastructure.common.userParameter.adapter;

import com.eni.netfleet.infrastructure.common.user.adapter.UserReadAdapter;
import com.eni.netfleet.infrastructure.common.userParameter.repository.UserParameterEntity;
import com.eni.netfleet.infrastructure.common.userParameter.repository.UserParameterRepository;
import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;
import com.eni.netfleet.metier.userParameter.port.UserParameterReadPort;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Validated
public class UserParameterReadAdapter implements UserParameterReadPort {

    private UserParameterRepository userParameterRepository;

    public UserParameterReadAdapter(final UserParameterRepository userParameterRepository) {
        this.userParameterRepository = userParameterRepository;
    }

    @Override
    public List<UserParameterBean> getAllUserParameters() {
        List<UserParameterEntity> userParameterEntities = userParameterRepository.findAll();
        return userParameterEntities.stream()
                .map(UserParameterReadAdapter::mapToUserParameterBean)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserParameterBean> getAllUserParametersByUserId(Long idUser) {
        List<UserParameterEntity> userParameterEntities = userParameterRepository.findAllByUserId(idUser);
        return userParameterEntities.stream()
                .map(UserParameterReadAdapter::mapToUserParameterBean)
                .collect(Collectors.toList());
    }

    public static UserParameterBean mapToUserParameterBean(UserParameterEntity userParameterEntity) {
        return UserParameterBean.builder()
                .id(userParameterEntity.getId())
                .createDate(userParameterEntity.getCreateDate())
                .updateDate(userParameterEntity.getUpdateDate())
                .user(UserReadAdapter.mapToUserBean(userParameterEntity.getUser()))
                .type(userParameterEntity.getType())
                .value(userParameterEntity.getValue())
                .build();
    }
}
