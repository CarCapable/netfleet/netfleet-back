package com.eni.netfleet.infrastructure.common.site.adapter;

import com.eni.netfleet.infrastructure.common.site.repository.SiteEntity;
import com.eni.netfleet.infrastructure.common.site.repository.SiteRepository;
import com.eni.netfleet.infrastructure.common.vehicule.adapter.VehicleReadAdapter;
import com.eni.netfleet.metier.site.bean.SiteBean;
import com.eni.netfleet.metier.site.port.SiteReadPort;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Validated
@RequiredArgsConstructor
public class SiteEntityReadAdapter implements SiteReadPort {

    private final SiteRepository siteRepository;

    @Override
    public List<SiteBean> getAllSites() {
        return siteRepository.findAll().stream().map(SiteEntityReadAdapter::mapToSiteBeanComplete).collect(Collectors.toList());
    }

    @Override
    public List<SiteBean> getAllSitesWihtoutVehicule() {

        return siteRepository.findAll().stream().map(SiteEntityReadAdapter::mapToSiteBean).collect(Collectors.toList());
    }

    public static SiteEntity mapToSiteEntity(SiteBean siteBean) {

        return SiteEntity.builder()
                .id(siteBean.getId())
                .city(siteBean.getCity())
                .number(siteBean.getNumber())
                .siteName(siteBean.getSiteName())
                .postalCode(siteBean.getPostalCode())
                .street(siteBean.getStreet())
                .build();
    }

    public static SiteBean mapToSiteBean(SiteEntity siteEntity) {

        return SiteBean.builder()
                .id(siteEntity.getId())
                .city(siteEntity.getCity())
                .number(siteEntity.getNumber())
                .siteName(siteEntity.getSiteName())
                .postalCode(siteEntity.getPostalCode())
                .street(siteEntity.getStreet())
                .build();
    }

    public static SiteBean mapToSiteBeanComplete(SiteEntity siteEntity) {

        List<VehicleBean> vehicleBeans = siteEntity.getVehicleEntities().stream().map(VehicleReadAdapter::mapToVehicleBean).collect(Collectors.toList());
        return SiteBean.builder()
                .city(siteEntity.getCity())
                .number(siteEntity.getNumber())
                .siteName(siteEntity.getSiteName())
                .postalCode(siteEntity.getPostalCode())
                .street(siteEntity.getStreet())
                .vehicleEntities(vehicleBeans)
                .build();
    }
}
