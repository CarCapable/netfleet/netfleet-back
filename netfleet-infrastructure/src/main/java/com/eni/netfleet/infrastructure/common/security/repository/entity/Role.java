package com.eni.netfleet.infrastructure.common.security.repository.entity;

import com.eni.netfleet.metier.user.constant.ERole;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;
import static com.eni.netfleet.infrastructure.common.DatabaseConstants.TABLES.ROLE;

@Entity
@Table(schema = SCHEMA, name = ROLE)
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;
}
