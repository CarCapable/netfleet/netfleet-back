package com.eni.netfleet.infrastructure.common.reservationRequest.adapter;

import com.eni.netfleet.infrastructure.common.SendEmail.SendEmail;
import com.eni.netfleet.infrastructure.common.place.adapter.PlaceReadAdapter;
import com.eni.netfleet.infrastructure.common.place.repository.PlaceRepository;
import com.eni.netfleet.infrastructure.common.reservationRequest.Projection.ReservationUsersWhosReserved;
import com.eni.netfleet.infrastructure.common.reservationRequest.repository.ReservationRequestEntity;
import com.eni.netfleet.infrastructure.common.reservationRequest.repository.ReservationRequestRepository;
import com.eni.netfleet.infrastructure.common.security.repository.UserRepository;
import com.eni.netfleet.infrastructure.common.user.adapter.UserReadAdapter;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import com.eni.netfleet.infrastructure.common.vehicule.adapter.VehicleReadAdapter;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleEntity;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleRepository;
import com.eni.netfleet.metier.reservationRequest.bean.*;
import com.eni.netfleet.metier.reservationRequest.port.ReservationRequestReadPort;
import com.sipios.springsearch.CriteriaParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Validated
public class ReservationRequestReadAdapter implements ReservationRequestReadPort {

    private ReservationRequestRepository reservationRequestRepository;
    private UserRepository userRepository;
    private PlaceRepository placeRepository;
    private VehicleRepository vehicleRepository;
    private SendEmail sendEmail;

    public ReservationRequestReadAdapter(final ReservationRequestRepository reservationRequestRepository,
                                         final UserRepository userRepository, final PlaceRepository placeRepository,
                                         final VehicleRepository vehicleRepository, SendEmail sendEmail) {
        this.reservationRequestRepository = reservationRequestRepository;
        this.userRepository = userRepository;
        this.placeRepository = placeRepository;
        this.vehicleRepository = vehicleRepository;
        this.sendEmail = sendEmail;
    }

    @Override
    public List<ReservationRequestBean> getAllReservationRequests(String search) {

        List<ReservationRequestEntity> reservationRequestEntities = search != null && !search.isEmpty() ? reservationRequestRepository.findAll(new CriteriaParser().parse(search)) : reservationRequestRepository.findAll();

        return reservationRequestEntities.stream()
                .map(ReservationRequestReadAdapter::mapToReservationRequestBean)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationRequestBean> getAllReservationRequestsForUser(Long idUser, String search) {
        List<ReservationRequestEntity> reservationRequestEntity = reservationRequestRepository.findAllByUserId(idUser);
        return reservationRequestEntity.stream()
                .map(ReservationRequestReadAdapter::mapToReservationRequestBean)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationRequestInfoBean> getAllDateReservationForVehicle(Long idVehicle) {
        List<ReservationRequestEntity> reservationRequestEntities = reservationRequestRepository.getAllDateReservationForVehicle(idVehicle);
        return reservationRequestEntities.stream().map(ReservationRequestReadAdapter::mapToReservationRequestInfoBean).collect(Collectors.toList());
    }

    @Override
    public List<ResponseUserWhosReserveVehicle> getAllUserWhosReservedThisVehicle(Long idVehicle) {
        List<ReservationUsersWhosReserved> usersWhosReserved = reservationRequestRepository.getAllUserWhosReservedThisVehicle(idVehicle);

        return mapToResponseUserWhosReserveVehicle(usersWhosReserved);
    }

    @Override
    public List<ReservationRequestBean> getAllReservationAlreadyExisting(ReservationRequestSearchBean reservationRequestSearchBean) {

        List<ReservationRequestEntity> reservationRequestEntity = reservationRequestRepository.getAllReservationAlreadyExisting(reservationRequestSearchBean.getPlaceId(), reservationRequestSearchBean.getStartDate(), reservationRequestSearchBean.getEndDate());
        return reservationRequestEntity.stream()
                .map(ReservationRequestReadAdapter::mapToReservationRequestBean)
                .filter(reservation -> reservation.getCarpoolPossible() && (reservation.getCarpoolUser().size() < reservation.getVehicle().getNbSeats()))
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<String> addRequestReservation(ReservationRequestAddBean reservationRequestAddBean) {

        Optional<VehicleEntity> vehicleEntity = vehicleRepository.findById(reservationRequestAddBean.getIdVehicle());
        Optional<UserEntity> userEntity = userRepository.findById(reservationRequestAddBean.getIdApplicant());

        if (userEntity.isPresent() && vehicleEntity.isPresent()) {
            ReservationRequestEntity reservationRequestEntity = ReservationRequestEntity.builder()
                    .applicant(userEntity.get())
                    .vehicle(vehicleEntity.get())
                    .carpoolPossible(reservationRequestAddBean.isCarpoolPossible())
                    .description(reservationRequestAddBean.getDescription())
                    .startDate(reservationRequestAddBean.getStartDate())
                    .endDate(reservationRequestAddBean.getEndDate())
                    .build();

            reservationRequestRepository.save(reservationRequestEntity);
            sendEmail.sendEmailConfirmation(userEntity.get().getUsername(), reservationRequestEntity.getApplicant().getIdentity().getEmail());
            return new ResponseEntity<>("The reservation was added succefully ! ", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Sorry an error has occured ! ", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public List<ReservationRequestBean> getAllCarpoolExisting(ReservationRequestCarpoolSearchBean reservationRequestCarpoolSearchBean) {
        List<ReservationRequestEntity> reservationRequestEntity = reservationRequestRepository.findAllCarpoolAvailableWithCriteria(reservationRequestCarpoolSearchBean.getPlaceId(), reservationRequestCarpoolSearchBean.getStartDate(), reservationRequestCarpoolSearchBean.getEndDate());
        return reservationRequestEntity.stream()
                .map(ReservationRequestReadAdapter::mapToReservationRequestBean)
                .collect(Collectors.toList());
    }

    @Override
    public void addCarpool(addCarpoolBean addCarpoolBean) {
        UserEntity userEntity = userRepository.getOne(addCarpoolBean.getIdUser());
        ReservationRequestEntity reservationRequestEntity = reservationRequestRepository.getOne(addCarpoolBean.getIdReservation());

        reservationRequestEntity.getCarpoolUser().add(userEntity);

        reservationRequestRepository.save(reservationRequestEntity);

        UserEntity sender = reservationRequestEntity.getApplicant();
        Optional<UserEntity> receiver = reservationRequestEntity.getCarpoolUser().stream().filter(user -> user.getId() == addCarpoolBean.getIdUser()).findFirst();

        sendEmail.sendEmailConfirmationCarpool(sender.getUsername(), sender.getIdentity().getEmail(), receiver.get().getUsername(), receiver.get().getIdentity().getEmail());
    }

    public static List<ResponseUserWhosReserveVehicle> mapToResponseUserWhosReserveVehicle(List<ReservationUsersWhosReserved> reservationUsersWhosReserveds) {
        return reservationUsersWhosReserveds
                .stream()
                .map(projection -> ResponseUserWhosReserveVehicle.builder()
                        .firstName(projection.getFirstName())
                        .lastName(projection.getName())
                        .build()).collect(Collectors.toList());
    }

    public static ReservationRequestBean mapToReservationRequestBean(ReservationRequestEntity reservationRequestEntity) {
        return ReservationRequestBean.builder()
                .id(reservationRequestEntity.getId())
                .createDate(reservationRequestEntity.getCreateDate())
                .updateDate(reservationRequestEntity.getUpdateDate())
                .applicant(UserReadAdapter.mapToUserBean(reservationRequestEntity.getApplicant()))
                .vehicle(VehicleReadAdapter.mapToVehicleBean(reservationRequestEntity.getVehicle()))
                .startDate(reservationRequestEntity.getStartDate())
                .endDate(reservationRequestEntity.getEndDate())
                .refusalReason(reservationRequestEntity.getRefusalReason())
                .carpoolPossible(reservationRequestEntity.getCarpoolPossible())
                .description(reservationRequestEntity.getDescription())
                .carpoolUser(reservationRequestEntity.getCarpoolUser().stream().map(UserReadAdapter::mapToUserBean).collect(Collectors.toList()))
                .build();
    }

    public static ReservationRequestInfoBean mapToReservationRequestInfoBean(ReservationRequestEntity reservationRequestEntity) {
        return ReservationRequestInfoBean.builder()
                .id(reservationRequestEntity.getId())
                .idCreatorReservation(reservationRequestEntity.getApplicant().getId())
                .nameApplicant(reservationRequestEntity.getApplicant().getIdentity().getName() + " " + reservationRequestEntity.getApplicant().getIdentity().getFirstName())
                .startDate(reservationRequestEntity.getStartDate())
                .endDate(reservationRequestEntity.getEndDate())
                .description(reservationRequestEntity.getDescription())
                .users(reservationRequestEntity.getCarpoolUser().stream().map(UserReadAdapter::mapToUserBean).collect(Collectors.toList()))
                .carpoolPossible(reservationRequestEntity.getCarpoolPossible())
                .nbPlace(reservationRequestEntity.getVehicle().getNbSeats())
                .build();
    }

}
