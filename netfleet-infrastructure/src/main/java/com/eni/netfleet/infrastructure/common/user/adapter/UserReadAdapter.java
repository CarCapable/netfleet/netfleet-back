package com.eni.netfleet.infrastructure.common.user.adapter;

import com.eni.netfleet.infrastructure.common.identity.adapter.IdentityReadAdapter;
import com.eni.netfleet.infrastructure.common.security.repository.UserRepository;
import com.eni.netfleet.infrastructure.common.security.repository.entity.Role;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import com.eni.netfleet.infrastructure.common.userParameter.adapter.UserParameterReadAdapter;
import com.eni.netfleet.metier.user.bean.RoleBean;
import com.eni.netfleet.metier.user.bean.UserBean;
import com.eni.netfleet.metier.user.port.UserReadPort;
import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Validated
public class UserReadAdapter implements UserReadPort {

    private UserRepository userRepository;

    private UserParameterReadAdapter userParameterReadAdapter;

    private PasswordEncoder encoder;

    public UserReadAdapter(final UserRepository userRepository, final UserParameterReadAdapter userParameterReadAdapter) {
        this.userRepository = userRepository;
        this.userParameterReadAdapter = userParameterReadAdapter;
    }

    @Override
    public List<UserBean> getAllUsers() {
        List<UserEntity> userEntities = userRepository.findAll();
        return userEntities.stream()
                .map(UserReadAdapter::mapToUserBean)
                .collect(Collectors.toList());
    }

    @Override
    public UserBean getUserById(Long idUser) {
        UserBean result = mapToUserBean(userRepository.findById(idUser).orElse(null));
        List<UserParameterBean> userParameters = userParameterReadAdapter.getAllUserParametersByUserId(idUser);
        if (Objects.nonNull(result) && !CollectionUtils.isEmpty(userParameters)) {
            result.setUserParameters(userParameters);
        }
        return result;
    }

    @Override
    public List<UserBean> getAllUsersByRole(String role) {
        List<UserEntity> userEntities = userRepository.findAllByRole(role);
        return userEntities.stream()
                .map(UserReadAdapter::mapToUserBean)
                .collect(Collectors.toList());
    }

    @Override
    public UserBean save(UserBean user) {
        if (Objects.isNull(user.getCreateDate())) {
            user.setCreateDate(new Date());
        }
        return mapToUserBean(userRepository.save(mapToUserEntity(user)));
    }

    public static UserBean mapToUserBean(UserEntity userEntity) {
        if (Objects.nonNull(userEntity)) {
            return UserBean.builder()
                    .id(userEntity.getId())
                    .createDate(userEntity.getCreateDate())
                    .updateDate(userEntity.getUpdateDate())
                    .identity(IdentityReadAdapter.mapToIdentityBean(userEntity.getIdentity()))
                    .username(userEntity.getUsername())
                    .password(userEntity.getPassword())
                    .status(userEntity.getStatus())
                    .roles(mapToRoleBeanSet(userEntity.getRoles()))
                    .build();
        }
        return null;
    }

    public static UserEntity mapToUserEntity(UserBean userBean) {
        if (Objects.nonNull(userBean)) {
            return UserEntity.builder()
                    .id(userBean.getId())
                    .createDate(userBean.getCreateDate())
                    .updateDate(userBean.getUpdateDate())
                    .identity(IdentityReadAdapter.mapToIdentityEntity(userBean.getIdentity()))
                    .username(userBean.getUsername())
                    .password(userBean.getPassword())
                    .status(userBean.getStatus())
                    .roles(mapToRoleSet(userBean.getRoles()))
                    .build();
        }
        return null;
    }

    private static Set<RoleBean> mapToRoleBeanSet(Set<Role> roles) {
        if (Objects.nonNull(roles)) {
            return roles.stream()
                    .map(UserReadAdapter::mapToRoleBean)
                    .collect(Collectors.toSet());
        } else {
            return null;
        }
    }

    private static Set<Role> mapToRoleSet(Set<RoleBean> roles) {
        if (Objects.nonNull(roles)) {
            return roles.stream()
                    .map(UserReadAdapter::mapToRole)
                    .collect(Collectors.toSet());
        } else {
            return null;
        }
    }

    private static RoleBean mapToRoleBean(Role role) {
        if (Objects.nonNull(role)) {
            return RoleBean.builder()
                    .id(role.getId())
                    .name(role.getName())
                    .build();
        } else {
            return null;
        }
    }

    private static Role mapToRole(RoleBean role) {
        if (Objects.nonNull(role)) {
            return Role.builder()
                    .id(role.getId())
                    .name(role.getName())
                    .build();
        } else {
            return null;
        }
    }
}

