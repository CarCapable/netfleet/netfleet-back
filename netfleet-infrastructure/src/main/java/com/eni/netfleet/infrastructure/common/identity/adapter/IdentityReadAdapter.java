package com.eni.netfleet.infrastructure.common.identity.adapter;

import com.eni.netfleet.infrastructure.common.identity.repository.IdentityEntity;
import com.eni.netfleet.infrastructure.common.identity.repository.IdentityRepository;
import com.eni.netfleet.metier.identity.bean.IdentityBean;
import com.eni.netfleet.metier.identity.port.IdentityReadPort;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Validated
public class IdentityReadAdapter implements IdentityReadPort {

    private IdentityRepository identityRepository;

    public IdentityReadAdapter(final IdentityRepository identityRepository) { this.identityRepository = identityRepository; }

    @Override
    public List<IdentityBean> getAllIdentities() {
        List<IdentityEntity> identityEntities = identityRepository.findAll();
        return identityEntities.stream()
                .map(IdentityReadAdapter::mapToIdentityBean)
                .collect(Collectors.toList());
    }

    public static IdentityBean mapToIdentityBean(IdentityEntity identityEntity) {
        return IdentityBean.builder()
                .id(identityEntity.getId())
                .createDate(identityEntity.getCreateDate())
                .updateDate(identityEntity.getUpdateDate())
                .name(identityEntity.getName())
                .firstName(identityEntity.getFirstName())
                .telephone(identityEntity.getTelephone())
                .email(identityEntity.getEmail())
                .build();
    }

    public static IdentityEntity mapToIdentityEntity(IdentityBean identityBean) {
        return IdentityEntity.builder()
                .id(identityBean.getId())
                .createDate(identityBean.getCreateDate())
                .updateDate(identityBean.getUpdateDate())
                .name(identityBean.getName())
                .firstName(identityBean.getFirstName())
                .telephone(identityBean.getTelephone())
                .email(identityBean.getEmail())
                .build();
    }
}
