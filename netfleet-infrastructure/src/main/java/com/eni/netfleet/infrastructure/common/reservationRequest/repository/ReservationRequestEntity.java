package com.eni.netfleet.infrastructure.common.reservationRequest.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.place.repository.PlaceEntity;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleEntity;
import com.eni.netfleet.metier.reservationRequest.constant.ReservationStatusEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;
import static com.eni.netfleet.infrastructure.common.DatabaseConstants.TABLES.CARPOOL_USERS;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.RESERVATION_REQUEST)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReservationRequestEntity extends AbstractIdentifierEntity {

    @Builder
    public ReservationRequestEntity(Long id, Date createDate, Date updateDate, UserEntity applicant, VehicleEntity vehicle, Date startDate, Date endDate, PlaceEntity departurePlace, PlaceEntity arrivalPlace, ReservationStatusEnum status, String refusalReason, Boolean carpoolPossible, String description, List<UserEntity> carpoolUser) {
        super(id, createDate, updateDate);
        this.applicant = applicant;
        this.vehicle = vehicle;
        this.startDate = startDate;
        this.endDate = endDate;
        this.departurePlace = departurePlace;
        this.arrivalPlace = arrivalPlace;
        this.status = status;
        this.refusalReason = refusalReason;
        this.carpoolPossible = carpoolPossible;
        this.description = description;
        this.carpoolUser = carpoolUser;
    }

    @ManyToOne
    @JoinColumn(name = "APPLICANT_REF", nullable = false)
    private UserEntity applicant;

    @ManyToOne
    @JoinColumn(name = "VEHICLE_REF", nullable = false)
    private VehicleEntity vehicle;

    @Temporal(TemporalType.DATE)
    @Column(name = "START_DATE", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "END_DATE", nullable = false)
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "DEPARTURE_PLACE_REF")
    private PlaceEntity departurePlace;

    @ManyToOne
    @JoinColumn(name = "ARRIVAL_PLACE_REF")
    private PlaceEntity arrivalPlace;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private ReservationStatusEnum status;

    @Column(name = "REFUSAL_REASON")
    private String refusalReason;

    @Column(name = "CARPOOL_POSSIBLE")
    private Boolean carpoolPossible;

    @Column(name = "DESCRIPTION", columnDefinition = "TEXT")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(schema = SCHEMA, name = CARPOOL_USERS, joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<UserEntity> carpoolUser;
}
