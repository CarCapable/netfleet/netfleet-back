package com.eni.netfleet.infrastructure.common.carpoolRequest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarpoolRequestRepository extends JpaRepository<CarpoolRequestEntity, Long> {
}
