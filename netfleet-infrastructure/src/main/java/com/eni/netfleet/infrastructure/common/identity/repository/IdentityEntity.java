package com.eni.netfleet.infrastructure.common.identity.repository;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;

import java.util.Date;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.IDENTITY)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class IdentityEntity extends AbstractIdentifierEntity {

    @Builder
    public IdentityEntity(Long id, Date createDate, Date updateDate, String name, String firstName, String telephone, @Size(max = 50) @Email String email) {
        super(id, createDate, updateDate);
        this.name = name;
        this.firstName = firstName;
        this.telephone = telephone;
        this.email = email;
    }

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "EMAIL", nullable = false)
    @Size(max = 50)
    @Email
    private String email;

}
