package com.eni.netfleet.infrastructure.common.security.repository;

import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByUsername(String username);

    Boolean existsByUsername(String username);

    @Query(value = " SELECT CASE " + " WHEN COUNT(us)> 0 then true else false end " + " FROM netfleet.user us " +
            " INNER JOIN netfleet.identity id ON us.identity_ref = id.id " + " where id.email = :email ", nativeQuery = true)
    Boolean ifExistsByEmail(@Param("email") String email);

    @Query(value = " SELECT * " + " FROM netfleet.user us " +
            " INNER JOIN netfleet.user_roles ur ON us.id = ur.user_id " +
            " INNER JOIN netfleet.roles roles ON ur.role_id = roles.id " +
            " where roles.name like :roleName ", nativeQuery = true)
    List<UserEntity> findAllByRole(@Param("roleName") String roleName);
}
