package com.eni.netfleet.infrastructure.common.reservationRequest.Projection;

public interface ReservationUsersWhosReserved {

    String getFirstName();

    String getName();
}
