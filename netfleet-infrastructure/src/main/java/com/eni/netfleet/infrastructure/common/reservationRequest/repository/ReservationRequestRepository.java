package com.eni.netfleet.infrastructure.common.reservationRequest.repository;

import com.eni.netfleet.infrastructure.common.reservationRequest.Projection.ReservationRequestDateProjection;
import com.eni.netfleet.infrastructure.common.reservationRequest.Projection.ReservationUsersWhosReserved;
import com.eni.netfleet.metier.reservationRequest.bean.ReservationRequestBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;

@RepositoryRestResource
public interface ReservationRequestRepository extends JpaRepository<ReservationRequestEntity, Long>, JpaSpecificationExecutor<ReservationRequestEntity> {

    @Query("SELECT rr FROM ReservationRequestEntity rr  " +
            "WHERE rr.vehicle.id = :idVehicule")
    List<ReservationRequestEntity> getAllDateReservationForVehicle(@Param("idVehicule") Long idVehicule);

    @Query("SELECT ie.name as name , ie.firstName as firstName  FROM ReservationRequestEntity rr " +
            "INNER JOIN UserEntity ue ON rr.applicant = ue.id " +
            "INNER JOIN VehicleEntity ve ON rr.vehicle = ve.id " +
            "INNER JOIN IdentityEntity ie ON ue.identity = ie.id " +
            "WHERE :idVehicule = ve.id " +
            "AND rr.status = 'TERMINATE' " +
            "ORDER BY rr.endDate")
    List<ReservationUsersWhosReserved> getAllUserWhosReservedThisVehicle(@Param("idVehicule") Long idVehicule);

    @Query("SELECT rR FROM ReservationRequestEntity rR " +
            "INNER JOIN PlaceEntity pe on rR.arrivalPlace = pe.id " +
            "WHERE pe.id = :arrivalPlaceId " +
            "AND rR.startDate = :dateDebut " +
            "AND rR.endDate = :dateFin")
    List<ReservationRequestEntity> getAllReservationAlreadyExisting(@Param("arrivalPlaceId") Long placeId, @Param("dateDebut") Date dateDebut, @Param("dateFin") Date dateFin);

    @Query("SELECT re FROM ReservationRequestEntity re Where re.applicant.id = :idUser Order by re.endDate")
    List<ReservationRequestEntity> findAllByUserId(@Param("idUser") Long idUser);

    @Query("SELECT re FROM ReservationRequestEntity re " +
            "INNER JOIN PlaceEntity pe on re.arrivalPlace = pe.id " +
            "WHERE (:idPlace is null or pe.id = :idPlace) " +
            "AND (:startDate is null or re.startDate >= :startDate) " +
            "AND (:endDate is null or re.endDate <= :endDate) " +
            "AND re.carpoolPossible = true")
    List<ReservationRequestEntity> findAllCarpoolAvailableWithCriteria(@Param("idPlace") Long idPlace, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
