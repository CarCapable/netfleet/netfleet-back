package com.eni.netfleet.infrastructure.common.place.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.identity.repository.IdentityEntity;
import lombok.*;

import javax.persistence.*;

import java.util.Date;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.PLACE)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PlaceEntity extends AbstractIdentifierEntity {

    @Builder
    public PlaceEntity(Long id, Date createDate, Date updateDate, String placeName, String number, String street, String city, String postalCode, IdentityEntity contact) {
        super(id, createDate, updateDate);
        this.placeName = placeName;
        this.number = number;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.contact = contact;
    }

    @Column(name = "PLACE_NAME")
    private String placeName;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "STREET")
    private String street;

    @Column(name = "CITY")
    private String city;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @ManyToOne
    @JoinColumn(name="CONTACT_REF")
    private IdentityEntity contact;
}
