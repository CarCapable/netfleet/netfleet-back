package com.eni.netfleet.infrastructure.common.reservationRequest.Projection;

import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;

import java.util.Date;

public interface ReservationRequestDateProjection {
    Date getStartDate();

    Date getEndDate();

    UserEntity getApplicant();

    String getDescription();

}
