package com.eni.netfleet.infrastructure.common.vehicule.adapter;

import com.eni.netfleet.infrastructure.common.site.adapter.SiteEntityReadAdapter;
import com.eni.netfleet.infrastructure.common.site.repository.SiteEntity;
import com.eni.netfleet.infrastructure.common.site.repository.SiteRepository;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleEntity;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleRepository;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import com.eni.netfleet.metier.vehicle.port.VehiculeWritePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

import static java.util.Objects.nonNull;

@Component
@Validated
@RequiredArgsConstructor
public class VehiculeWriteAdapter implements VehiculeWritePort {

    private final VehicleRepository vehicleRepository;

    private final SiteRepository siteRepository;


    @Override
    public void createVehicule(VehicleBean vehicule) {
        vehicleRepository.save(Objects.requireNonNull(mapToVehiculeEntity(vehicule)));
    }

    @Override
    public void updateVehicule(VehicleBean vehicle) {

        VehicleEntity vehicleUpdate = Objects.requireNonNull(mapToVehiculeEntity(vehicle));
        SiteEntity siteUpdate = siteRepository.getOne(vehicleUpdate.getSiteEntity().getId());

        VehicleEntity vehicleInDB = vehicleRepository.getOne(vehicleUpdate.getId());

        vehicleInDB.setBrand(vehicleUpdate.getBrand());
        vehicleInDB.setFuel(vehicleUpdate.getFuel());
        vehicleInDB.setImage(vehicleUpdate.getImage());
        vehicleInDB.setKeysPosition(vehicleUpdate.getKeysPosition());
        vehicleInDB.setModel(vehicleUpdate.getModel());
        vehicleInDB.setNbSeats(vehicleUpdate.getNbSeats());
        vehicleInDB.setSiteEntity(siteUpdate);
        vehicleInDB.setStatus(vehicleUpdate.getStatus());
        vehicleInDB.setStickGear(vehicleUpdate.getStickGear());
        vehicleInDB.setTrunkCapacity(vehicleUpdate.getTrunkCapacity());
        vehicleInDB.setVinCode(vehicleUpdate.getVinCode());

        vehicleRepository.save(vehicleInDB);
    }

    public static VehicleEntity mapToVehiculeEntity(VehicleBean vehicleBean) {
        if (nonNull(vehicleBean)) {

            return VehicleEntity.builder()
                    .id(vehicleBean.getId())
                    .brand(vehicleBean.getBrand())
                    .fuel(vehicleBean.getFuel())
                    .model(vehicleBean.getModel())
                    .nbSeats(vehicleBean.getNbSeats())
                    .status(vehicleBean.getStatut())
                    .stickGear(vehicleBean.getStickGear())
                    .trunkCapacity(vehicleBean.getTrunkCapacity())
                    .vinCode(vehicleBean.getVinCode())
                    .image(vehicleBean.getImage())
                    .keysPosition(vehicleBean.getKeysPosition())
                    .siteEntity(SiteEntityReadAdapter.mapToSiteEntity(vehicleBean.getSite()))
                    .build();
        }
        return null;
    }
}
