package com.eni.netfleet.infrastructure.common.reservationRequest.Projection;

import java.util.Date;

public interface ReservationDetailHistory {
    Long getId();

    Date getStartDate();

    String getStatus();

    String getRefusalReason();

    Boolean getCarpoolingPossible();

    String getDescription();

    String getDeparturePlaceName();

    String getDepartureNumber();

    String getDepartureStreet();

    String getDepartureCity();

    String getDeparturePostalCode();

    String getArrivalPlaceName();

    String getArrivalNumber();

    String getArrivalStreet();

    String getArrivalCity();

    String getArrivalPostalCode();

    String getBrand();

    String getModel();

    String getVehicleStatus();

    String getFuel();

    String getNbSeats();

    String getTruckCapacity();

    String getStickGear();
}
