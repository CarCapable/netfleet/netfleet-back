package com.eni.netfleet.infrastructure.common.reservationRequest.Projection;

import java.util.Date;

public interface ReservationHistoricView {
    Long getId();

    String getBrand();

    String getModel();

    Date getStartDate();

    String getDeparturePlaceName();

    String getArrivalPlaceName();

    String getStatus();
}
