package com.eni.netfleet.infrastructure.common.vehicule.repository;

import com.eni.netfleet.metier.vehicle.constant.VehicleStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface VehicleRepository extends JpaRepository<VehicleEntity, Long>, JpaSpecificationExecutor<VehicleEntity> {


    @Modifying
    @Query("UPDATE VehicleEntity SET status = :statusEnum WHERE id = :id ")
    void disabledVehicleById(@Param("id") Long id, @Param("statusEnum") VehicleStatusEnum statusEnum);

    @Modifying
    @Query("UPDATE VehicleEntity ve SET brand = :vehicule, fuel = :vehicuel where id = :vehicule ")
    void updateVehicule(@Param("vehicule") VehicleEntity vehicleEntity);
}
