package com.eni.netfleet.infrastructure.common.userParameter.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import com.eni.netfleet.metier.userParameter.constant.UserParameterTypeEnum;
import lombok.*;

import javax.persistence.*;

import java.util.Date;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.USER_PARAMETER)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserParameterEntity extends AbstractIdentifierEntity {

    @Builder
    public UserParameterEntity(Long id, Date createDate, Date updateDate, UserEntity user, UserParameterTypeEnum type, String value) {
        super(id, createDate, updateDate);
        this.user = user;
        this.type = type;
        this.value = value;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="USER_REF", nullable = false)
    private UserEntity user;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserParameterTypeEnum type;

    @Column(name = "VALUE", nullable = false)
    private String value;
}
