package com.eni.netfleet.infrastructure.common.SendEmail;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class SendEmail {

    private JavaMailSender emailSender;

    public SendEmail(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void sendEmailConfirmation(String userName, String email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Reservation confirmation");
        message.setText("Hello : " + userName + " ! your reservation has been made !");
        emailSender.send(message);
    }

    public void sendEmailConfirmationCarpool(String userNameSend, String emailSend, String userNameReceiver, String emailReceiver) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailSend);
        message.setSubject("Reservation confirmation");
        message.setText("Hello : " + userNameSend + " ! your carpool has been made !");
        emailSender.send(message);

        SimpleMailMessage messageReceiver = new SimpleMailMessage();
        messageReceiver.setTo(emailReceiver);
        messageReceiver.setSubject("Reservation confirmation");
        messageReceiver.setText("Hello : " + userNameSend + " ! " + userNameReceiver + " decide to carpool with you !");
        emailSender.send(messageReceiver);
    }
}
