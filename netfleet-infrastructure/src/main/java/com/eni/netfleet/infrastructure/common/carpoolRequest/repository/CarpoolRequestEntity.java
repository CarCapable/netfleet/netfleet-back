package com.eni.netfleet.infrastructure.common.carpoolRequest.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.reservationRequest.repository.ReservationRequestEntity;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.CARPOOL_REQUEST)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CarpoolRequestEntity extends AbstractIdentifierEntity {

    @Builder
    public CarpoolRequestEntity(Long id, Date createDate, Date updateDate, UserEntity applicant, ReservationRequestEntity reservationRequest) {
        super(id, createDate, updateDate);
        this.applicant = applicant;
        this.reservationRequest = reservationRequest;
    }

    @ManyToOne
    @JoinColumn(name = "APPLICANT_REF", nullable = false)
    private UserEntity applicant;

    @ManyToOne
    @JoinColumn(name = "RESERVATION_REQUEST_REF", nullable = false)
    private ReservationRequestEntity reservationRequest;

}
