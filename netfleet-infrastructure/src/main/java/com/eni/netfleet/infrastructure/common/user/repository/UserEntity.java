package com.eni.netfleet.infrastructure.common.user.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.identity.repository.IdentityEntity;
import com.eni.netfleet.infrastructure.common.security.repository.entity.Role;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.USER)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserEntity extends AbstractIdentifierEntity {

    @Builder
    public UserEntity(Long id, Date createDate, Date updateDate, Set<Role> roles, @NonNull IdentityEntity identity, @NonNull String username, @NonNull String password, Boolean status) {
        super(id, createDate, updateDate);
        this.roles = roles;
        this.identity = identity;
        this.username = username;
        this.password = password;
        this.status = status;
    }

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    @JoinColumn(name = "IDENTITY_REF", referencedColumnName = "ID", nullable = false)
    private IdentityEntity identity;

    @Column(name = "USERNAME", unique = true)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "STATUS")
    private Boolean status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = SCHEMA, name = DatabaseConstants.TABLES.USER_ROLE,
            joinColumns = @JoinColumn(name = "USER_ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<Role> roles = new HashSet<>();

}
