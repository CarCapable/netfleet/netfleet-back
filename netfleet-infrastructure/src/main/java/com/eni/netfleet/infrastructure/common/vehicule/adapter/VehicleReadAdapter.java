package com.eni.netfleet.infrastructure.common.vehicule.adapter;

import com.eni.netfleet.infrastructure.common.site.adapter.SiteEntityReadAdapter;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleEntity;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleRepository;
import com.eni.netfleet.metier.vehicle.bean.DisableVehicleBean;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import com.eni.netfleet.metier.vehicle.constant.VehicleStatusEnum;
import com.eni.netfleet.metier.vehicle.port.VehicleReadPort;
import com.sipios.springsearch.CriteriaParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Validated
@RequiredArgsConstructor
public class VehicleReadAdapter implements VehicleReadPort {

    private final VehicleRepository vehicleRepository;

    @Override
    public List<VehicleBean> getAllVehicles(String search) {

        List<VehicleEntity> vehicleEntities = search != null && !search.isEmpty() ? vehicleRepository.findAll(new CriteriaParser().parse(search)) : vehicleRepository.findAll();

        return vehicleEntities.stream()
                .map(VehicleReadAdapter::mapToVehicleBean)
                .collect(Collectors.toList());
    }

    @Override
    public VehicleBean getVehicleById(final Long id) {

        return mapToVehicleBean(vehicleRepository.getOne(id));
    }

    @Override
    public void disabledVehicleById(Long id, DisableVehicleBean disableVehicleBean) {
        vehicleRepository.disabledVehicleById(id, disableVehicleBean.getStatusEnum());
    }

    @Override
    public List<VehicleBean> getAllVehiclesAvailable(String search) {
        List<VehicleBean> vehicles = getAllVehicles(search);

        return vehicles.stream().filter(vehicle -> vehicle.getStatut().equals(VehicleStatusEnum.AVAILABLE)).collect(Collectors.toList());
    }

    public static VehicleBean mapToVehicleBean(VehicleEntity vehicleEntity) {

        return VehicleBean.builder()
                .id(vehicleEntity.getId())
                .createDate(vehicleEntity.getCreateDate())
                .updateDate(vehicleEntity.getUpdateDate())
                .vinCode(vehicleEntity.getVinCode())
                .brand(vehicleEntity.getBrand())
                .model(vehicleEntity.getModel())
                .statut(vehicleEntity.getStatus())
                .fuel(vehicleEntity.getFuel())
                .nbSeats(vehicleEntity.getNbSeats())
                .trunkCapacity(vehicleEntity.getTrunkCapacity())
                .stickGear(vehicleEntity.getStickGear())
                .image(vehicleEntity.getImage())
                .keysPosition(vehicleEntity.getKeysPosition())
                .site(SiteEntityReadAdapter.mapToSiteBean(vehicleEntity.getSiteEntity()))
                .build();
    }
}
