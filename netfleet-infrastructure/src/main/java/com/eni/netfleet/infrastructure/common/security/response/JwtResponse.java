package com.eni.netfleet.infrastructure.common.security.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JwtResponse {
    private String token;

    private final String type = "Bearer";

    private Long id;

    private String username;

    private String email;

    private List<String> roles;
}
