package com.eni.netfleet.infrastructure.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Constantes liées au schéma de base de données (schéma, tables...)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DatabaseConstants {

    public static final String SCHEMA = "netfleet";

    /**
     * Constantes des noms de tables
     */
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class TABLES {
        public static final String VEHICLE = "VEHICLE";
        public static final String IDENTITY = "IDENTITY";
        public static final String USER_PARAMETER = "USER_PARAMETER";
        public static final String USER = "USER";
        public static final String ROLE = "ROLES";
        public static final String USER_ROLE = "USER_ROLES";
        public static final String PLACE = "PLACE";
        public static final String RESERVATION_REQUEST = "RESERVATION_REQUEST";
        public static final String CARPOOL_REQUEST = "CARPOOL_REQUEST";
        public static final String CARPOOL_USERS = "CARPOOL_USERS";
        public static final String SITE = "SITE";
    }

}
