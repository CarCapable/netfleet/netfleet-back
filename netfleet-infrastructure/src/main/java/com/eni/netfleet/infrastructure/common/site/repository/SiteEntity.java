package com.eni.netfleet.infrastructure.common.site.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.vehicule.repository.VehicleEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.SITE)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SiteEntity extends AbstractIdentifierEntity {

    @Builder
    public SiteEntity(Long id, Date createDate, Date updateDate, String siteName, String number, String street, String city, String postalCode, List<VehicleEntity> vehicleEntities) {
        super(id, createDate, updateDate);
        this.siteName = siteName;
        this.number = number;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.vehicleEntities = vehicleEntities;
    }

    @Column(name = "siteName")
    private String siteName;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "STREET")
    private String street;

    @Column(name = "CITY")
    private String city;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @OneToMany(targetEntity = VehicleEntity.class, mappedBy = "siteEntity")
    private List<VehicleEntity> vehicleEntities;

}