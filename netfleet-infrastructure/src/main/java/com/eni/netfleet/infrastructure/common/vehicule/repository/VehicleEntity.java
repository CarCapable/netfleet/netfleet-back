package com.eni.netfleet.infrastructure.common.vehicule.repository;

import com.eni.netfleet.infrastructure.common.DatabaseConstants;
import com.eni.netfleet.infrastructure.common.abstractIdentifier.AbstractIdentifierEntity;
import com.eni.netfleet.infrastructure.common.site.repository.SiteEntity;
import com.eni.netfleet.metier.vehicle.constant.VehicleFuelEnum;
import com.eni.netfleet.metier.vehicle.constant.VehicleStatusEnum;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

import static com.eni.netfleet.infrastructure.common.DatabaseConstants.SCHEMA;

@Entity
@Table(schema = SCHEMA, name = DatabaseConstants.TABLES.VEHICLE)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class VehicleEntity extends AbstractIdentifierEntity {


    @Builder
    public VehicleEntity(Long id, Date createDate, Date updateDate, String vinCode, String brand, String model, VehicleStatusEnum status, VehicleFuelEnum fuel, Integer nbSeats, Float trunkCapacity, Boolean stickGear, String keysPosition, SiteEntity siteEntity, String image) {
        super(id, createDate, updateDate);
        this.vinCode = vinCode;
        this.brand = brand;
        this.model = model;
        this.status = status;
        this.fuel = fuel;
        this.nbSeats = nbSeats;
        this.trunkCapacity = trunkCapacity;
        this.stickGear = stickGear;
        this.keysPosition = keysPosition;
        this.siteEntity = siteEntity;
        this.image = image;
    }

    @Column(name = "VIN_CODE")
    private String vinCode;

    @Column(name = "BRAND", nullable = false)
    private String brand;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private VehicleStatusEnum status;

    @Column(name = "FUEL", nullable = false)
    @Enumerated(EnumType.STRING)
    private VehicleFuelEnum fuel;

    @Column(name = "NB_SEATS", nullable = false)
    private Integer nbSeats;

    @Column(name = "TRUNK_CAPACITY")
    private Float trunkCapacity;

    @Column(name = "STICK_GEAR", nullable = false)
    private Boolean stickGear;

    @Column(name = "KEYS_POSITION")
    private String keysPosition;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "SITE_REF")
    private SiteEntity siteEntity;

    @Column(name = "IMAGE", nullable = false)
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String image;
}
