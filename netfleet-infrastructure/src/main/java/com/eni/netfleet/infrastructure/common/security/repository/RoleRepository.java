package com.eni.netfleet.infrastructure.common.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.eni.netfleet.infrastructure.common.security.repository.entity.Role;
import com.eni.netfleet.metier.user.constant.ERole;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
