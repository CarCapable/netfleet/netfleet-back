package com.eni.netfleet.infrastructure.common.user.adapter;

import com.eni.netfleet.infrastructure.common.identity.adapter.IdentityReadAdapter;
import com.eni.netfleet.infrastructure.common.security.repository.UserRepository;
import com.eni.netfleet.infrastructure.common.security.repository.entity.Role;
import com.eni.netfleet.infrastructure.common.user.repository.UserEntity;
import com.eni.netfleet.infrastructure.common.userParameter.adapter.UserParameterReadAdapter;
import com.eni.netfleet.metier.user.bean.RoleBean;
import com.eni.netfleet.metier.user.bean.UserBean;
import com.eni.netfleet.metier.user.port.UserWritePort;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Validated
public class UserWriteAdapter implements UserWritePort {

    private final UserRepository userRepository;

    static private PasswordEncoder encoder;

    public UserWriteAdapter(final UserRepository userRepository, final PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Override
    public void createUser(UserBean user) {
        userRepository.save(Objects.requireNonNull(mapToUserEntity(user)));
    }

    @Override
    public void updateUser(UserBean user) {
        UserEntity userUpdate = Objects.requireNonNull(UserReadAdapter.mapToUserEntity(user));

        userRepository.save(userUpdate);
    }

    public static UserEntity mapToUserEntity(UserBean userBean) {
        if (Objects.nonNull(userBean)) {
            return UserEntity.builder()
                    .id(userBean.getId())
                    .createDate(userBean.getCreateDate())
                    .updateDate(userBean.getUpdateDate())
                    .identity(IdentityReadAdapter.mapToIdentityEntity(userBean.getIdentity()))
                    .username(userBean.getUsername())
                    .password(encoder.encode(userBean.getPassword()))
                    .status(userBean.getStatus())
                    .roles(mapToRoleSet(userBean.getRoles()))
                    .build();
        }
        return null;
    }

    private static Set<Role> mapToRoleSet(Set<RoleBean> roles) {
        if (Objects.nonNull(roles)) {
            return roles.stream()
                    .map(UserWriteAdapter::mapToRole)
                    .collect(Collectors.toSet());
        } else {
            return null;
        }
    }

    private static Role mapToRole(RoleBean role) {
        if (Objects.nonNull(role)) {
            return Role.builder()
                    .id(role.getId())
                    .name(role.getName())
                    .build();
        } else {
            return null;
        }
    }

}
