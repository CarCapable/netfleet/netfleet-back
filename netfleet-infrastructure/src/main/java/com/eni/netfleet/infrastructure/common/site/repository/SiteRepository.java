package com.eni.netfleet.infrastructure.common.site.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SiteRepository extends JpaRepository<SiteEntity, Long> {
}
