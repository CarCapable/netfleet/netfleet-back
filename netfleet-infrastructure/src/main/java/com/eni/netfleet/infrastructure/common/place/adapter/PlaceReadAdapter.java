package com.eni.netfleet.infrastructure.common.place.adapter;

import com.eni.netfleet.infrastructure.common.identity.adapter.IdentityReadAdapter;
import com.eni.netfleet.infrastructure.common.place.repository.PlaceEntity;
import com.eni.netfleet.infrastructure.common.place.repository.PlaceRepository;
import com.eni.netfleet.metier.place.bean.PlaceBean;
import com.eni.netfleet.metier.place.port.PlaceReadPort;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Validated
public class PlaceReadAdapter implements PlaceReadPort {

    private PlaceRepository placeRepository;

    public PlaceReadAdapter(final PlaceRepository placeRepository) { this.placeRepository = placeRepository; }

    @Override
    public List<PlaceBean> getAllPlaces() {
        List<PlaceEntity> placeEntities = placeRepository.findAll();
        return placeEntities.stream()
                .map(PlaceReadAdapter::mapToPlaceBean)
                .collect(Collectors.toList());
    }

    public static PlaceBean mapToPlaceBean(PlaceEntity placeEntity) {
        return PlaceBean.builder()
                .id(placeEntity.getId())
                .placeName(placeEntity.getPlaceName())
                .number(placeEntity.getNumber())
                .street(placeEntity.getStreet())
                .city(placeEntity.getCity())
                .postalCode(placeEntity.getPostalCode())
                .contact(IdentityReadAdapter.mapToIdentityBean(placeEntity.getContact()))
                .build();
    }
}
