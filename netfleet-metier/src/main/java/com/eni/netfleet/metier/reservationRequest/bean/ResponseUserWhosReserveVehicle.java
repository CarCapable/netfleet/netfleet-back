package com.eni.netfleet.metier.reservationRequest.bean;

import lombok.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = PRIVATE)
public class ResponseUserWhosReserveVehicle {

    @NonNull
    String firstName;

    @NonNull
    String lastName;
}
