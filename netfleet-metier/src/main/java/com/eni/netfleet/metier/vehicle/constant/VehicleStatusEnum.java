package com.eni.netfleet.metier.vehicle.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VehicleStatusEnum {
    AVAILABLE("AVAILABLE"),
    BOOKED("BOOKED"),
    UNAVAILABLE("UNAVAILABLE");

    @JsonProperty("value")
    private String value;

    VehicleStatusEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static VehicleStatusEnum fromValue(final String value) {
        if (value != null) {
            for (VehicleStatusEnum type : VehicleStatusEnum.values()) {
                if (value.equalsIgnoreCase(type.value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
