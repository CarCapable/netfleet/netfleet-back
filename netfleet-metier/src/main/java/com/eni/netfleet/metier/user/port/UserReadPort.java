package com.eni.netfleet.metier.user.port;

import com.eni.netfleet.metier.user.bean.UserBean;

import java.util.List;

public interface UserReadPort {

    /**
     * Get all user available on dataBase
     *
     * @return list of users
     */
    List<UserBean> getAllUsers();

    /**
     * Get user by id
     *
     * @return user
     */
    UserBean getUserById(Long idUser);

    /**
     * Get all user available on dataBase by role
     *
     * @return list of users
     */
    List<UserBean> getAllUsersByRole(String role);

    /**
     * Save user
     *
     * @return saved user
     */
    UserBean save(UserBean user);
}
