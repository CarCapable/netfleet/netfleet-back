package com.eni.netfleet.metier.reservationRequest.bean;

import lombok.*;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class ReservationRequestSearchBean {

    @NonNull
    Date startDate;

    @NonNull
    Date endDate;

    @NonNull
    Long placeId;
}
