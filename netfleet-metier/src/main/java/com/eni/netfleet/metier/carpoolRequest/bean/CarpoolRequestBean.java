package com.eni.netfleet.metier.carpoolRequest.bean;

import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import com.eni.netfleet.metier.reservationRequest.bean.ReservationRequestBean;
import com.eni.netfleet.metier.reservationRequest.constant.ReservationStatusEnum;
import com.eni.netfleet.metier.user.bean.UserBean;
import lombok.*;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class CarpoolRequestBean extends AbstractIdentifierBean {

    @Builder
    public CarpoolRequestBean(@NonNull Long id, Date createDate, Date updateDate, @NonNull ReservationStatusEnum status, @NonNull UserBean applicant, @NonNull ReservationRequestBean reservationRequest, @NonNull Integer nbPeople) {
        super(id, createDate, updateDate);
        this.status = status;
        this.applicant = applicant;
        this.reservationRequest = reservationRequest;
        this.nbPeople = nbPeople;
    }

    @NonNull
    private ReservationStatusEnum status;

    @NonNull
    private UserBean applicant;

    @NonNull
    private ReservationRequestBean reservationRequest;

    @NonNull
    private Integer nbPeople;
}
