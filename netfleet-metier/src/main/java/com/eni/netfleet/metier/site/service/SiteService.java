package com.eni.netfleet.metier.site.service;

import com.eni.netfleet.metier.site.bean.SiteBean;

import java.util.List;

public interface SiteService {

    /**
     * Get all site available on dataBase
     *
     * @return list of sites
     */
    List<SiteBean> getAllSites();


    /**
     * Get all site available on dataBase
     *
     * @return list of sites withtout vehicle
     */
    List<SiteBean> getAllSitesWihtoutVehicule();
}
