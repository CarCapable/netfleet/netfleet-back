package com.eni.netfleet.metier.place.service;

import com.eni.netfleet.metier.place.bean.PlaceBean;
import com.eni.netfleet.metier.place.port.PlaceReadPort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
public class PlaceServiceImpl implements PlaceService {

    private final PlaceReadPort placeReadPort;

    public PlaceServiceImpl(final PlaceReadPort placeReadPort) { this.placeReadPort = placeReadPort; }

    @Override
    public List<PlaceBean> getAllPlaces() {
        return placeReadPort.getAllPlaces();
    }
}
