package com.eni.netfleet.metier.user.bean;

import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import com.eni.netfleet.metier.identity.bean.IdentityBean;
import com.eni.netfleet.metier.user.constant.ERole;
import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;
import lombok.*;

import java.util.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class UserBean extends AbstractIdentifierBean {

    @Builder
    public UserBean(Long id, Date createDate, Date updateDate, Set<RoleBean> roles, @NonNull IdentityBean identity, @NonNull String username, @NonNull String password, Boolean status, List<UserParameterBean> userParameters) {
        super(id, createDate, updateDate);
        this.roles = roles;
        this.identity = identity;
        this.username = username;
        this.password = password;
        this.status = status;
        this.userParameters = userParameters;
    }

    private Set<RoleBean> roles = new HashSet<>();

    @NonNull
    private IdentityBean identity;

    @NonNull
    private String username;

    @NonNull
    private String password;

    private Boolean status;

    private List<UserParameterBean> userParameters = new ArrayList<>();
}
