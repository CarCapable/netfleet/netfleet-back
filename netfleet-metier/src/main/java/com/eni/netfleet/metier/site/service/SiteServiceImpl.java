package com.eni.netfleet.metier.site.service;

import com.eni.netfleet.metier.site.bean.SiteBean;
import com.eni.netfleet.metier.site.port.SiteReadPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
@RequiredArgsConstructor
public class SiteServiceImpl implements SiteService {

    private final SiteReadPort siteReadPort;

    @Override
    public List<SiteBean> getAllSites() {
        return siteReadPort.getAllSites();
    }

    @Override
    public List<SiteBean> getAllSitesWihtoutVehicule() {
        return siteReadPort.getAllSitesWihtoutVehicule();
    }
}
