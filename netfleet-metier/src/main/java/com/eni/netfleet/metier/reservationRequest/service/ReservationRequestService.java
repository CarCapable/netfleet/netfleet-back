package com.eni.netfleet.metier.reservationRequest.service;

import com.eni.netfleet.metier.reservationRequest.bean.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ReservationRequestService {

    /**
     * Get all reservation request available on dataBase
     *
     * @return list of reservation requests
     */
    List<ReservationRequestBean> getAllReservationRequests(String search);

    /**
     * Get all reservation request available on database for one user specified
     *
     * @param idUser id of user
     * @param search criteria of search
     * @return all dates for one user.
     */
    List<ReservationRequestBean> getAllReservationRequestsForUser(Long idUser, String search);

    /**
     * Get all date where the vehicle is reserve
     *
     * @param idVehicle id of vehicle
     * @return all dates
     */
    List<ReservationRequestInfoBean> getAllDateReservationForVehicle(Long idVehicle);

    /**
     * Get all user who's reserved the vehicle this vehicle
     *
     * @param idVehicle id of vehicle
     * @return user who's reserved this vehicle
     */
    List<ResponseUserWhosReserveVehicle> getAllUserWhosReservedThisVehicle(Long idVehicle);


    /**
     * Get all reservation already existing at this date and this place
     *
     * @param reservationRequestSearchBean
     * @return all reservation already existing
     */
    List<ReservationRequestBean> getAllReservationAlreadyExisting(ReservationRequestSearchBean reservationRequestSearchBean);


    /**
     * Add a new reservation request
     *
     * @param reservationRequestAddBean
     * @return a response about if it's ok
     */
    ResponseEntity<String> addRequestReservation(ReservationRequestAddBean reservationRequestAddBean);

    /**
     * Get all carpool already existing
     *
     * @param reservationRequestCarpoolSearchBean criteria of search
     * @return all reservation request with carpool available
     */
    List<ReservationRequestBean> getAllCarpoolExisting(ReservationRequestCarpoolSearchBean reservationRequestCarpoolSearchBean);

    /**
     * add Carpool
     *
     * @param addCarpoolBean criteria of search
     */
    void addCarpool(addCarpoolBean addCarpoolBean);
}
