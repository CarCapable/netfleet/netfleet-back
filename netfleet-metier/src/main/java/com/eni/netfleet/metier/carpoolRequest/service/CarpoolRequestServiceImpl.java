package com.eni.netfleet.metier.carpoolRequest.service;

import com.eni.netfleet.metier.carpoolRequest.bean.CarpoolRequestBean;
import com.eni.netfleet.metier.carpoolRequest.port.CarpoolRequestReadPort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
public class CarpoolRequestServiceImpl implements CarpoolRequestService {

    private final CarpoolRequestReadPort carpoolRequestReadPort;

    public CarpoolRequestServiceImpl(final CarpoolRequestReadPort carpoolRequestReadPort) {
        this.carpoolRequestReadPort = carpoolRequestReadPort;
    }

    @Override
    public List<CarpoolRequestBean> getAllCarpoolRequests() {
        return carpoolRequestReadPort.getAllCarpoolRequests();
    }
}
