package com.eni.netfleet.metier.place.bean;

import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import com.eni.netfleet.metier.identity.bean.IdentityBean;
import lombok.*;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class PlaceBean extends AbstractIdentifierBean {

    @Builder
    public PlaceBean(@NonNull Long id, Date createDate, Date updateDate, String placeName, String number, String street, String city, String postalCode, IdentityBean contact) {
        super(id, createDate, updateDate);
        this.placeName = placeName;
        this.number = number;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.contact = contact;
    }

    private String placeName;

    private String number;

    private String street;

    private String city;

    private String postalCode;

    private IdentityBean contact;
}
