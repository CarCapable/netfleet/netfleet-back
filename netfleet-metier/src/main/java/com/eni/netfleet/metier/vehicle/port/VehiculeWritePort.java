package com.eni.netfleet.metier.vehicle.port;

import com.eni.netfleet.metier.vehicle.bean.VehicleBean;

public interface VehiculeWritePort {

    /**
     * Create new vehicule
     *
     * @param vehicule
     */
    void createVehicule(VehicleBean vehicule);

    /**
     * Update vehicule
     *
     * @param vehicle
     */
    void updateVehicule(VehicleBean vehicle);
}
