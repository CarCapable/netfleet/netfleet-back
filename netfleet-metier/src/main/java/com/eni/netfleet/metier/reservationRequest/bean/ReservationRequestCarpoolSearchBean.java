package com.eni.netfleet.metier.reservationRequest.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class ReservationRequestCarpoolSearchBean {

    private Long placeId;

    private Date startDate;

    private Date endDate;


}
