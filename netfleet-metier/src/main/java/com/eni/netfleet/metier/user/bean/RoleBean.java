package com.eni.netfleet.metier.user.bean;

import com.eni.netfleet.metier.user.constant.ERole;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class RoleBean {

    @Builder
    public RoleBean(@NonNull Integer id, @NonNull ERole name) {
        this.id = id;
        this.name = name;
    }

    @NonNull
    private Integer id;

    @NonNull
    private ERole name;
}
