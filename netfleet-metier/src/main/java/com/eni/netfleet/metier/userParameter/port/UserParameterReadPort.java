package com.eni.netfleet.metier.userParameter.port;

import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;

import java.util.List;

public interface UserParameterReadPort {

    /**
     * Get all user parameters available on dataBase
     *
     * @return list of user parameters
     */
    List<UserParameterBean> getAllUserParameters();

    /**
     * Get all user parameters available on dataBase by user id
     *
     * @return list of user parameters
     */
    List<UserParameterBean> getAllUserParametersByUserId(Long idUser);
}
