package com.eni.netfleet.metier.vehicle.bean;

import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import com.eni.netfleet.metier.site.bean.SiteBean;
import com.eni.netfleet.metier.vehicle.constant.VehicleFuelEnum;
import com.eni.netfleet.metier.vehicle.constant.VehicleStatusEnum;
import lombok.*;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class VehicleBean extends AbstractIdentifierBean {

    @Builder
    public VehicleBean(Long id, Date createDate, Date updateDate, String vinCode, @NonNull String brand, String model, @NonNull VehicleStatusEnum statut, @NonNull VehicleFuelEnum fuel, @NonNull Integer nbSeats, Float trunkCapacity, @NonNull Boolean stickGear, @NonNull String image, @NonNull String keysPosition, @NonNull SiteBean site) {
        super(id, createDate, updateDate);
        this.vinCode = vinCode;
        this.brand = brand;
        this.model = model;
        this.statut = statut;
        this.fuel = fuel;
        this.nbSeats = nbSeats;
        this.trunkCapacity = trunkCapacity;
        this.stickGear = stickGear;
        this.image = image;
        this.keysPosition = keysPosition;
        this.site = site;
    }

    private String vinCode;

    @NonNull
    private String brand;

    private String model;

    @NonNull
    private VehicleStatusEnum statut;

    @NonNull
    private VehicleFuelEnum fuel;

    @NonNull
    private Integer nbSeats;

    private Float trunkCapacity;

    @NonNull
    private Boolean stickGear;

    @NonNull
    private String image;

    @NonNull
    private String keysPosition;

    @NonNull
    private SiteBean site;
}
