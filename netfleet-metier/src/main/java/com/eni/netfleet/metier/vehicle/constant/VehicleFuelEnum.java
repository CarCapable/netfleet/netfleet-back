package com.eni.netfleet.metier.vehicle.constant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VehicleFuelEnum {
    PETROL("PETROL"),
    DIESEL("DIESEL");

    @JsonProperty("value")
    private String value;

    VehicleFuelEnum(String value) {
        this.value = value;
    }

    @JsonCreator
    public static VehicleFuelEnum fromValue(final String value) {
        if (value != null) {
            for (VehicleFuelEnum type : VehicleFuelEnum.values()) {
                if (value.equalsIgnoreCase(type.value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
