package com.eni.netfleet.metier.identity.bean;

import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import lombok.*;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class IdentityBean extends AbstractIdentifierBean {

    @Builder
    public IdentityBean(Long id, Date createDate, Date updateDate, @NonNull String name, @NonNull String firstName, String telephone, @NonNull String email) {
        super(id, createDate, updateDate);
        this.name = name;
        this.firstName = firstName;
        this.telephone = telephone;
        this.email = email;
    }

    @NonNull
    private String name;

    @NonNull
    private String firstName;

    private String telephone;

    @NonNull
    private String email;
}
