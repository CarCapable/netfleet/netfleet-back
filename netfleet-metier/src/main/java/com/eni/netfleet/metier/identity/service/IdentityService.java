package com.eni.netfleet.metier.identity.service;

import com.eni.netfleet.metier.identity.bean.IdentityBean;

import java.util.List;

public interface IdentityService {

    /**
     * Get all car available on dataBase
     *
     * @return list of identities
     */
    List<IdentityBean> getAllIdentities();
}
