package com.eni.netfleet.metier.userParameter.bean;

import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import com.eni.netfleet.metier.user.bean.UserBean;
import com.eni.netfleet.metier.userParameter.constant.UserParameterTypeEnum;
import lombok.*;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class UserParameterBean extends AbstractIdentifierBean {

    @Builder
    public UserParameterBean(@NonNull Long id, Date createDate, Date updateDate, @NonNull UserBean user, @NonNull UserParameterTypeEnum type, @NonNull String value) {
        super(id, createDate, updateDate);
        this.user = user;
        this.type = type;
        this.value = value;
    }

    @NonNull
    private UserBean user;

    @NonNull
    private UserParameterTypeEnum type;

    @NonNull
    private String value;
}
