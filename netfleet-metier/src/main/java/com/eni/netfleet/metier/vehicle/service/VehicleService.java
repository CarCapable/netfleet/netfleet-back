package com.eni.netfleet.metier.vehicle.service;

import com.eni.netfleet.metier.vehicle.bean.DisableVehicleBean;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;

import java.util.List;

public interface VehicleService {

    /**
     * Get all vehicle available on dataBase
     *
     * @return list of vehicles
     */
    List<VehicleBean> getAllVehicles(String search);

    /**
     * Get vehicle by id
     *
     * @param id of vehicle
     * @return vehicle
     */
    VehicleBean getVehicleById(Long id);

    /**
     * Disable vehicle by id
     *
     * @param id                 of vehicle
     * @param disableVehicleBean
     */
    void disabledVehicleById(Long id, DisableVehicleBean disableVehicleBean);

    /**
     * Get all vehicles available for reservation
     *
     * @return vehicles available
     */
    List<VehicleBean> getAllVehiclesAvailable(String search);

    /**
     * Create new vehicule
     *
     * @param vehicule
     */
    void createVehicule(VehicleBean vehicule);

    /**
     * update vehicule
     *
     * @param vehicule
     */
    void updateVehicule(VehicleBean vehicule);

}
