package com.eni.netfleet.metier.vehicle.service;

import com.eni.netfleet.metier.vehicle.bean.DisableVehicleBean;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import com.eni.netfleet.metier.vehicle.port.VehicleReadPort;
import com.eni.netfleet.metier.vehicle.port.VehiculeWritePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private final VehicleReadPort vehicleReadPort;

    private final VehiculeWritePort vehiculeWritePort;

    @Override
    public List<VehicleBean> getAllVehicles(String search) {

        return vehicleReadPort.getAllVehicles(search);
    }

    @Override
    public VehicleBean getVehicleById(final Long id) {

        return vehicleReadPort.getVehicleById(id);
    }

    @Override
    public void disabledVehicleById(Long id, DisableVehicleBean disableVehicleBean) {
        vehicleReadPort.disabledVehicleById(id, disableVehicleBean);
    }

    @Override
    public List<VehicleBean> getAllVehiclesAvailable(String search) {
        return vehicleReadPort.getAllVehiclesAvailable(search);
    }

    @Override
    public void createVehicule(VehicleBean vehicule) {
        vehiculeWritePort.createVehicule(vehicule);
    }

    @Override
    public void updateVehicule(VehicleBean vehicule) {
        vehiculeWritePort.updateVehicule(vehicule);
    }
}
