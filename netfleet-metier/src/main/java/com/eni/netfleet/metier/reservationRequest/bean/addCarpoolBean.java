package com.eni.netfleet.metier.reservationRequest.bean;

import lombok.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
@Builder
public class addCarpoolBean {

    @NonNull
    Long idReservation;

    @NonNull
    Long idUser;

}
