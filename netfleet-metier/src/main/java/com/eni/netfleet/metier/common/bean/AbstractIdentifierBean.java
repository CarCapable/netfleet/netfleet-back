package com.eni.netfleet.metier.common.bean;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractIdentifierBean {

    private Long id;

    private Date createDate;

    private Date updateDate;
}
