package com.eni.netfleet.metier.reservationRequest.constant;

public enum ReservationStatusEnum {

    PENDING,
    ACCEPTED,
    REFUSED,
    CANCELLED
}
