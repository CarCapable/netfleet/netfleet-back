package com.eni.netfleet.metier.reservationRequest.bean;

import com.eni.netfleet.metier.carpoolRequest.bean.CarpoolRequestBean;
import com.eni.netfleet.metier.common.bean.AbstractIdentifierBean;
import com.eni.netfleet.metier.place.bean.PlaceBean;
import com.eni.netfleet.metier.reservationRequest.constant.ReservationStatusEnum;
import com.eni.netfleet.metier.user.bean.UserBean;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import lombok.*;

import java.util.Date;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class ReservationRequestBean extends AbstractIdentifierBean {

    @Builder
    public ReservationRequestBean(@NonNull Long id, Date createDate, Date updateDate, @NonNull UserBean applicant,
                                  @NonNull VehicleBean vehicle, @NonNull Date startDate, @NonNull Date endDate,
                                  PlaceBean departurePlace, PlaceBean arrivalPlace,
                                  ReservationStatusEnum status, String refusalReason, Boolean carpoolPossible,
                                  String description, List<CarpoolRequestBean> carpoolRequests,
                                  List<UserBean> carpoolUser) {
        super(id, createDate, updateDate);
        this.applicant = applicant;
        this.vehicle = vehicle;
        this.startDate = startDate;
        this.endDate = endDate;
        this.departurePlace = departurePlace;
        this.arrivalPlace = arrivalPlace;
        this.status = status;
        this.refusalReason = refusalReason;
        this.carpoolPossible = carpoolPossible;
        this.description = description;
        this.carpoolRequests = carpoolRequests;
        this.carpoolUser = carpoolUser;
    }

    @NonNull
    private UserBean applicant;

    @NonNull
    private VehicleBean vehicle;

    @NonNull
    private Date startDate;

    @NonNull
    private Date endDate;

    @NonNull
    private PlaceBean departurePlace;

    @NonNull
    private PlaceBean arrivalPlace;

    @NonNull
    private ReservationStatusEnum status;

    private String refusalReason;

    private Boolean carpoolPossible;

    private String description;

    private List<UserBean> carpoolUser;

    private List<CarpoolRequestBean> carpoolRequests;
}
