package com.eni.netfleet.metier.userParameter.service;

import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;

import java.util.List;

public interface UserParameterService {

    /**
     * Get all user parameter available on dataBase
     *
     * @return list of user parameters
     */
    List<UserParameterBean> getAllUserParameters();
}
