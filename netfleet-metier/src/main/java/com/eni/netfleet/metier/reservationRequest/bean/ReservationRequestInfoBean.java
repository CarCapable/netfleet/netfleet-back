package com.eni.netfleet.metier.reservationRequest.bean;

import com.eni.netfleet.metier.user.bean.UserBean;
import lombok.*;

import java.util.Date;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = PRIVATE)
public class ReservationRequestInfoBean {

    @NonNull
    Long id;

    @NonNull
    Date startDate;

    @NonNull
    Date endDate;

    @NonNull
    String nameApplicant;

    @NonNull
    String description;

    @NonNull
    Long idCreatorReservation;

    @NonNull
    List<UserBean> users ;

    @NonNull
    boolean carpoolPossible;

    @NonNull
    int nbPlace;
}
