package com.eni.netfleet.metier.identity.service;

import com.eni.netfleet.metier.identity.bean.IdentityBean;
import com.eni.netfleet.metier.identity.port.IdentityReadPort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
public class IdentityServiceImpl implements IdentityService {

    private final IdentityReadPort identityReadPort;

    public IdentityServiceImpl(final IdentityReadPort identityReadPort) { this.identityReadPort = identityReadPort; }

    @Override
    public List<IdentityBean> getAllIdentities() {
        return identityReadPort.getAllIdentities();
    }
}
