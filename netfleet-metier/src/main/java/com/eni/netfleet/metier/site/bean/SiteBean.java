package com.eni.netfleet.metier.site.bean;

import com.eni.netfleet.metier.vehicle.bean.VehicleBean;
import lombok.*;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor(access = PRIVATE)
public class SiteBean {

    private Long id;

    private String siteName;

    private String number;

    private String street;

    private String city;

    private String postalCode;

    private List<VehicleBean> vehicleEntities;
}
