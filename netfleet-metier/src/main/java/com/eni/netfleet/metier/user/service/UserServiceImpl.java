package com.eni.netfleet.metier.user.service;

import com.eni.netfleet.metier.user.bean.UserBean;
import com.eni.netfleet.metier.user.port.UserReadPort;
import com.eni.netfleet.metier.user.port.UserWritePort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
public class UserServiceImpl implements UserService {

    private final UserReadPort userReadPort;

    private final UserWritePort userWritePort;

    public UserServiceImpl(final UserReadPort userReadPort, final UserWritePort userWritePort) {
        this.userReadPort = userReadPort;
        this.userWritePort = userWritePort;
    }

    @Override
    public List<UserBean> getAllUsers() {
        return userReadPort.getAllUsers();
    }

    @Override
    public UserBean getUserById(Long idUser) {
        return userReadPort.getUserById(idUser);
    }

    @Override
    public List<UserBean> getAllUsersByRole(String role) {
        return userReadPort.getAllUsersByRole(role);
    }

    @Override
    public UserBean save(UserBean user) {
        return userReadPort.save(user);
    }

    @Override
    public void updateUser(UserBean user) {
        userWritePort.updateUser(user);
    }

    @Override
    public void createUser(UserBean user) {
        userWritePort.createUser(user);
    }
}
