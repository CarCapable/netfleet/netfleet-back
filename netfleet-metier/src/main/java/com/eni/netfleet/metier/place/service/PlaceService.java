package com.eni.netfleet.metier.place.service;

import com.eni.netfleet.metier.place.bean.PlaceBean;

import java.util.List;

public interface PlaceService {

    /**
     * Get all place available on dataBase
     *
     * @return list of places
     */
    List<PlaceBean> getAllPlaces();
}
