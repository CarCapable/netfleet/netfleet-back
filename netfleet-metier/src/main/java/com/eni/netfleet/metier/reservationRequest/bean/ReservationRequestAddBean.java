package com.eni.netfleet.metier.reservationRequest.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class ReservationRequestAddBean {

    boolean carpoolPossible;

    @NonNull
    String description;

    @NonNull
    Date endDate;

    @NonNull
    Date startDate;

    @NonNull
    Long idVehicle;

    @NonNull
    Long idApplicant;

}
