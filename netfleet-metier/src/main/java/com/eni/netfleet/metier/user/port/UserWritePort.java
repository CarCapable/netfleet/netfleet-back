package com.eni.netfleet.metier.user.port;

import com.eni.netfleet.metier.user.bean.UserBean;

public interface UserWritePort {

    /**
     * Create new user
     *
     * @param user
     */
    void createUser(UserBean user);

    /**
     * Update user
     *
     * @param user
     */
    void updateUser(UserBean user);
}
