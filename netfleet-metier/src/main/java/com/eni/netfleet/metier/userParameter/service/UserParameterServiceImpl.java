package com.eni.netfleet.metier.userParameter.service;

import com.eni.netfleet.metier.userParameter.bean.UserParameterBean;
import com.eni.netfleet.metier.userParameter.port.UserParameterReadPort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
public class UserParameterServiceImpl implements UserParameterService {

    private final UserParameterReadPort userParameterReadPort;

    public UserParameterServiceImpl(final UserParameterReadPort userParameterReadPort) {
        this.userParameterReadPort = userParameterReadPort;
    }

    @Override
    public List<UserParameterBean> getAllUserParameters() {
        return userParameterReadPort.getAllUserParameters();
    }
}
