package com.eni.netfleet.metier.carpoolRequest.service;

import com.eni.netfleet.metier.carpoolRequest.bean.CarpoolRequestBean;

import java.util.List;

public interface CarpoolRequestService {
    /**
     * Get all carpool request available on dataBase
     *
     * @return list of carpool requests
     */
    List<CarpoolRequestBean> getAllCarpoolRequests();

}
