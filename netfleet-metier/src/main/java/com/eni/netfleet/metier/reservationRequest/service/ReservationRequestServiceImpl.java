package com.eni.netfleet.metier.reservationRequest.service;

import com.eni.netfleet.metier.reservationRequest.bean.*;
import com.eni.netfleet.metier.reservationRequest.port.ReservationRequestReadPort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
@Validated
public class ReservationRequestServiceImpl implements ReservationRequestService {

    private final ReservationRequestReadPort reservationRequestReadPort;

    public ReservationRequestServiceImpl(final ReservationRequestReadPort reservationRequestReadPort) {
        this.reservationRequestReadPort = reservationRequestReadPort;
    }

    @Override
    public List<ReservationRequestBean> getAllReservationRequests(String search) {
        return reservationRequestReadPort.getAllReservationRequests(search);
    }

    @Override
    public List<ReservationRequestBean> getAllReservationRequestsForUser(Long idUser, String search) {
        return reservationRequestReadPort.getAllReservationRequestsForUser(idUser, search);
    }

    @Override
    public List<ReservationRequestInfoBean> getAllDateReservationForVehicle(Long idVehicle) {
        return reservationRequestReadPort.getAllDateReservationForVehicle(idVehicle);
    }

    @Override
    public List<ResponseUserWhosReserveVehicle> getAllUserWhosReservedThisVehicle(Long idVehicle) {
        return reservationRequestReadPort.getAllUserWhosReservedThisVehicle(idVehicle);
    }

    @Override
    public List<ReservationRequestBean> getAllReservationAlreadyExisting(ReservationRequestSearchBean reservationRequestSearchBean) {
        return reservationRequestReadPort.getAllReservationAlreadyExisting(reservationRequestSearchBean);
    }

    @Override
    public ResponseEntity<String> addRequestReservation(ReservationRequestAddBean reservationRequestAddBean) {
        return reservationRequestReadPort.addRequestReservation(reservationRequestAddBean);
    }

    @Override
    public List<ReservationRequestBean> getAllCarpoolExisting(ReservationRequestCarpoolSearchBean reservationRequestCarpoolSearchBean) {
        return reservationRequestReadPort.getAllCarpoolExisting(reservationRequestCarpoolSearchBean);
    }

    @Override
    public void addCarpool(addCarpoolBean addCarpoolBean) {
        reservationRequestReadPort.addCarpool(addCarpoolBean);
    }
}
