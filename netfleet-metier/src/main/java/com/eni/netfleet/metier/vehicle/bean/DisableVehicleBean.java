package com.eni.netfleet.metier.vehicle.bean;

import com.eni.netfleet.metier.vehicle.constant.VehicleStatusEnum;
import lombok.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
public class DisableVehicleBean {

    @NonNull
    VehicleStatusEnum statusEnum;
}
