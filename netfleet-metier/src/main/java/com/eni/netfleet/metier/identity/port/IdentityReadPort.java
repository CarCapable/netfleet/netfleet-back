package com.eni.netfleet.metier.identity.port;

import com.eni.netfleet.metier.identity.bean.IdentityBean;

import java.util.List;

public interface IdentityReadPort {

    /**
     * Get all car available on dataBase
     *
     * @return list of identities
     */
    List<IdentityBean> getAllIdentities();
}
