package com.eni.netfleet.metier.vehicle.port;

import com.eni.netfleet.metier.vehicle.bean.DisableVehicleBean;
import com.eni.netfleet.metier.vehicle.bean.VehicleBean;

import java.util.List;

public interface VehicleReadPort {

    /**
     * Get all car available on dataBase
     *
     * @return list of cars
     */
    List<VehicleBean> getAllVehicles(String search);

    /**
     * Get vehicle by id
     *
     * @param id of vehicle
     * @return vehicle
     */
    VehicleBean getVehicleById(Long id);

    /**
     * Disable vehicle by id
     * @param id of vehicle
     * @param disableVehicleBean
     */
    void disabledVehicleById(Long id, DisableVehicleBean disableVehicleBean);

    /**
     * Get all vehicles available for reservation
     *
     * @return vehicles available
     */
    List<VehicleBean> getAllVehiclesAvailable(String search);

}
