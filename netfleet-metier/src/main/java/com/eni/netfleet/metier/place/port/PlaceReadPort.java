package com.eni.netfleet.metier.place.port;

import com.eni.netfleet.metier.place.bean.PlaceBean;

import java.util.List;

public interface PlaceReadPort {

    /**
     * Get all car available on dataBase
     *
     * @return list of places
     */
    List<PlaceBean> getAllPlaces();
}
